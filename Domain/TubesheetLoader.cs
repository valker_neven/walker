﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Xml;
using Walker.Model;


namespace Walker.Domain
{
	
	public interface ITubesheetLoader
	{
		Tubesheet LoadTubesheet();
	}
	
	public class TubesheetXMLLoader : ITubesheetLoader
	{
		
		string _fileName;
		public TubesheetXMLLoader(string xmlFileName) {
			_fileName = xmlFileName;
			if (!File.Exists(_fileName)) {
				throw new Exception($"{_fileName} does not exist!");
			}
		}
		
		public TubeStatus GetTubeStatusFromString(string status) {
			switch (status) {
				case "Plugged":
					return TubeStatus.Plugged;
				case "Critical":
					return TubeStatus.Critical;
				default:
					return TubeStatus.Unknown;
			}
		}

		public Tubesheet LoadTubesheet() {
			List<Tube> tubes = new List<Tube>();
			XmlDocument doc = new XmlDocument();
			doc.Load(_fileName);
			XmlNode pitchNode = doc.SelectSingleNode("/TubesheetModel/TubesheetPitch");
			XmlNode diameterNode = doc.SelectSingleNode("/TubesheetModel/TubesheetDiameter");
			XmlNode tubesNode = doc.SelectSingleNode("/TubesheetModel/Tubes");
			foreach (XmlNode tubeNode in tubesNode.ChildNodes) {
				int row = int.Parse(tubeNode.SelectSingleNode("Row").InnerText);
				int column = int.Parse(tubeNode.SelectSingleNode("Column").InnerText);
				string status =  tubeNode.SelectSingleNode("Status").InnerText;
				TubeStatus ts = GetTubeStatusFromString(status);
				Tube tube = new Tube(row, column, ts);
				tubes.Add(tube);
			}
			double pitch = double.Parse(pitchNode.InnerText, CultureInfo.InvariantCulture);
			double diameter = double.Parse(diameterNode.InnerText, CultureInfo.InvariantCulture);
			return new Tubesheet(tubes, diameter, pitch);
		}
	}
}