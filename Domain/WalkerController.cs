﻿using Walker.Model;
using System.Windows;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Walker.Domain
{
	public class WalkerController
	{
		protected WalkerModel _walkerModel;
		protected Tubesheet _tubesheet;
		protected WalkerPosition _walkerPosition;

		public WalkerController(WalkerModel walker, Tubesheet tubesheet) {
			_walkerModel = walker;
			_tubesheet = tubesheet;
		}

		public WalkerPosition GetDefaultPosition() {
			WalkerPosition pos = new WalkerPosition(_walkerModel);
			pos.Orientation = ToolHeadOrientation.upLeft;
			int headTubeColIndex = 0;

			if (_walkerModel.PrimaryAxisLengthInPitchesD < _walkerModel.SecondaryAxisLengthInPitches) {
				headTubeColIndex = (int)Math.Round((_walkerModel.SecondaryAxisLengthInPitches - _walkerModel.PrimaryAxisLengthInPitchesD) * 0.5);
			}
			pos.ToolHeadTube = _tubesheet.GetTube(_tubesheet.RowsCount, headTubeColIndex);
			pos.PrimaryAxisTranslation = 0;
			pos.SecondaryAxisTranslation = 0;
			pos.PrimaryLocked = true;
			pos.SecondaryLocked = true;

			pos.Rotation = -45;
			return pos;
		}

		public WalkerPositionOnTubes GetPositionOnTubes(WalkerPosition walkerPosition) {
			if (!walkerPosition.IsValid()) {
				return null;
			}
			int TH_R = walkerPosition.ToolHeadTube.RowIndex;
			int TH_C = walkerPosition.ToolHeadTube.ColumnIndex;
			int PLD = _walkerModel.PrimaryAxisLengthInPitchesD;
			int SL = _walkerModel.SecondaryAxisLengthInPitches;
			int PAxT1_R = -1;
			int PAxT1_C = -1;
			int PAxT2_R = -1;
			int PAxT2_C = -1;
			int SAxT1_R = -1;
			int SAxT1_C = -1;
			int SAxT2_R = -1;
			int SAxT2_C = -1;
			if (walkerPosition.Orientation == ToolHeadOrientation.upLeft) {
				PAxT1_R = TH_R - 1;
				PAxT1_C = TH_C + 1;
				PAxT2_R = PAxT1_R - PLD;
				PAxT2_C = PAxT1_C + PLD;
				if (walkerPosition.Rotation == -45) {
					SAxT1_R = PAxT2_R + (PAxT1_R - PAxT2_R) / 2;
					SAxT1_C = PAxT1_C + (PAxT2_C - PAxT1_C) / 2 - SL / 2;
					SAxT2_R = PAxT2_R + (PAxT1_R - PAxT2_R) / 2;
					SAxT2_C = PAxT1_C + (PAxT2_C - PAxT1_C) / 2 + SL / 2;
				}
				else if (walkerPosition.Rotation == 45) {
					SAxT1_R = PAxT2_R + (PAxT1_R - PAxT2_R) / 2 - SL / 2;
					SAxT1_C = PAxT1_C + (PAxT2_C - PAxT1_C) / 2;
					SAxT2_R = PAxT2_R + (PAxT1_R - PAxT2_R) / 2 + SL / 2;
					SAxT2_C = PAxT1_C + (PAxT2_C - PAxT1_C) / 2;
				}
			}
			else if (walkerPosition.Orientation == ToolHeadOrientation.upRight) {
				PAxT1_R = TH_R - 1;
				PAxT1_C = TH_C - 1;
				PAxT2_R = PAxT1_R - PLD;
				PAxT2_C = PAxT1_C - PLD;
				if (walkerPosition.Rotation == -45) {
					SAxT1_R = PAxT2_R + (PAxT1_R - PAxT2_R) / 2 + SL / 2;
					SAxT1_C = PAxT2_C + (PAxT1_C - PAxT2_C) / 2;
					SAxT2_R = PAxT2_R + (PAxT1_R - PAxT2_R) / 2 - SL / 2;
					SAxT2_C = PAxT2_C + (PAxT1_C - PAxT2_C) / 2;
				}
				else if (walkerPosition.Rotation == 45) {
					SAxT1_R = PAxT2_R + (PAxT1_R - PAxT2_R) / 2;
					SAxT1_C = PAxT2_C + (PAxT1_C - PAxT2_C) / 2 - SL / 2;
					SAxT2_R = PAxT2_R + (PAxT1_R - PAxT2_R) / 2;
					SAxT2_C = PAxT2_C + (PAxT1_C - PAxT2_C) / 2 + SL / 2;
				}
			}
			else if (walkerPosition.Orientation == ToolHeadOrientation.downLeft) {
				PAxT1_R = TH_R + 1;
				PAxT1_C = TH_C + 1;
				PAxT2_R = PAxT1_R + PLD;
				PAxT2_C = PAxT1_C + PLD;
				if (walkerPosition.Rotation == -45) {
					SAxT1_R = PAxT1_R + (PAxT2_R - PAxT1_R) / 2 - SL / 2;
					SAxT1_C = PAxT2_C + (PAxT1_C - PAxT2_C) / 2;
					SAxT2_R = PAxT1_R + (PAxT2_R - PAxT1_R) / 2 + SL / 2;
					SAxT2_C = PAxT2_C + (PAxT1_C - PAxT2_C) / 2;
				}
				else if (walkerPosition.Rotation == 45) {
					SAxT1_R = PAxT1_R + (PAxT2_R - PAxT1_R) / 2;
					SAxT1_C = PAxT2_C + (PAxT1_C - PAxT2_C) / 2 + SL / 2;
					SAxT2_R = PAxT1_R + (PAxT2_R - PAxT1_R) / 2;
					SAxT2_C = PAxT2_C + (PAxT1_C - PAxT2_C) / 2 - SL / 2;
				}
			}
			else {
				PAxT1_R = TH_R + 1;
				PAxT1_C = TH_C - 1;
				PAxT2_R = PAxT1_R + PLD;
				PAxT2_C = PAxT1_C - PLD;
				if (walkerPosition.Rotation == -45) {
					SAxT1_R = PAxT1_R + (PAxT2_R - PAxT1_R) / 2;
					SAxT1_C = PAxT1_C + (PAxT2_C - PAxT1_C) / 2 + SL / 2;
					SAxT2_R = PAxT1_R + (PAxT2_R - PAxT1_R) / 2;
					SAxT2_C = PAxT1_C + (PAxT2_C - PAxT1_C) / 2 - SL / 2;
				}
				else if (walkerPosition.Rotation == 45) {
					SAxT1_R = PAxT1_R + (PAxT2_R - PAxT1_R) / 2 + SL / 2;
					SAxT1_C = PAxT2_C + (PAxT1_C - PAxT2_C) / 2;
					SAxT2_R = PAxT1_R + (PAxT2_R - PAxT1_R) / 2 - SL / 2;
					SAxT2_C = PAxT2_C + (PAxT1_C - PAxT2_C) / 2;
				}
			}
			if (walkerPosition.PrimaryAxisTranslation != 0) {
				int pitchesOffset = GetPrimaryOffsetPitches(walkerPosition);
				if (SAxT1_R == SAxT2_R) {
					if (SAxT1_C < SAxT2_C) {
						pitchesOffset = -pitchesOffset;
					}
					SAxT1_C = SAxT1_C + pitchesOffset;
					SAxT2_C = SAxT2_C + pitchesOffset;
				}
				else {
					if (SAxT1_R < SAxT2_R) {
						pitchesOffset = -pitchesOffset;
					}
					SAxT1_R = SAxT1_R + pitchesOffset;
					SAxT2_R = SAxT2_R + pitchesOffset;
				}
			}
			if (walkerPosition.SecondaryAxisTranslation != 0) {
				int pitchesOffset = GetSecondaryOffsetPitches(walkerPosition);
				if (walkerPosition.Orientation == ToolHeadOrientation.upLeft) {
					SAxT1_R = SAxT1_R - pitchesOffset;
					SAxT1_C = SAxT1_C + pitchesOffset;
					SAxT2_R = SAxT2_R - pitchesOffset;
					SAxT2_C = SAxT2_C + pitchesOffset;
				}
				else if (walkerPosition.Orientation == ToolHeadOrientation.upRight) {
					SAxT1_R = SAxT1_R - pitchesOffset;
					SAxT1_C = SAxT1_C - pitchesOffset;
					SAxT2_R = SAxT2_R - pitchesOffset;
					SAxT2_C = SAxT2_C - pitchesOffset;
				}
				else if (walkerPosition.Orientation == ToolHeadOrientation.downRight) {
					SAxT1_R = SAxT1_R + pitchesOffset;
					SAxT1_C = SAxT1_C - pitchesOffset;
					SAxT2_R = SAxT2_R + pitchesOffset;
					SAxT2_C = SAxT2_C - pitchesOffset;
				}
				else {
					SAxT1_R = SAxT1_R + pitchesOffset;
					SAxT1_C = SAxT1_C + pitchesOffset;
					SAxT2_R = SAxT2_R + pitchesOffset;
					SAxT2_C = SAxT2_C + pitchesOffset;
				}
			}
			return new WalkerPositionOnTubes(walkerPosition, GetTube(PAxT1_R, PAxT1_C), GetTube(PAxT2_R, PAxT2_C), GetTube(SAxT1_R, SAxT1_C), GetTube(SAxT2_R, SAxT2_C));
			;
		}

		protected int GetPrimaryOffsetPitches(WalkerPosition position) {
			return (int)Math.Round((position.PrimaryAxisTranslation / _tubesheet.TubesheetPitch), 0);
		}

		protected int GetSecondaryOffsetPitches(WalkerPosition position) {
			return (int)Math.Round(position.SecondaryAxisTranslation / Math.Sqrt(2) / _tubesheet.TubesheetPitch, 0);
		}

		public Tube GetTube(int rowIndex, int columnIndex) {
			return _tubesheet.GetTube(rowIndex, columnIndex);
		}

		public WalkerPosition WalkerPosition {
			get {
				return _walkerPosition;
			}
			set {
				_walkerPosition = value;
			}
		}

		public WalkerPosition OffsetPrimary(WalkerPosition position, int offset) {
			WalkerPosition newPos = position.Clone();
			newPos.PrimaryAxisTranslation = position.PrimaryAxisTranslation + offset * _tubesheet.TubesheetPitch;
			if (position.Orientation == ToolHeadOrientation.upLeft) {
				if (position.Rotation == -45) {
					newPos.ToolHeadTube = _tubesheet.GetTube(position.ToolHeadTube.RowIndex, position.ToolHeadTube.ColumnIndex + offset);
				}
				else {
					newPos.ToolHeadTube = _tubesheet.GetTube(position.ToolHeadTube.RowIndex + offset, position.ToolHeadTube.ColumnIndex);
				}
			}
			else if (position.Orientation == ToolHeadOrientation.upRight) {
				if (position.Rotation == -45) {
					newPos.ToolHeadTube = _tubesheet.GetTube(position.ToolHeadTube.RowIndex - offset, position.ToolHeadTube.ColumnIndex);
				}
				else {
					newPos.ToolHeadTube = _tubesheet.GetTube(position.ToolHeadTube.RowIndex, position.ToolHeadTube.ColumnIndex + offset);
				}
			}
			else if (position.Orientation == ToolHeadOrientation.downRight) {
				if (position.Rotation == -45) {
					newPos.ToolHeadTube = _tubesheet.GetTube(position.ToolHeadTube.RowIndex, position.ToolHeadTube.ColumnIndex - offset);
				}
				else {
					newPos.ToolHeadTube = _tubesheet.GetTube(position.ToolHeadTube.RowIndex - offset, position.ToolHeadTube.ColumnIndex);
				}
			}
			else {
				if (position.Rotation == -45) {
					newPos.ToolHeadTube = _tubesheet.GetTube(position.ToolHeadTube.RowIndex + offset, position.ToolHeadTube.ColumnIndex);
				}
				else {
					newPos.ToolHeadTube = _tubesheet.GetTube(position.ToolHeadTube.RowIndex, position.ToolHeadTube.ColumnIndex - offset);
				}
			}
			return newPos;
		}

		public WalkerPosition OffsetSecondaryByPrimary(WalkerPosition position, int offset) {
			WalkerPosition newPos = position.Clone();
			newPos.PrimaryAxisTranslation = newPos.PrimaryAxisTranslation - offset * _tubesheet.TubesheetPitch;
			return newPos;
		}

		public WalkerPosition OffsetSecondary(WalkerPosition position, int offset) {
			WalkerPosition newPos = position.Clone();
			newPos.SecondaryAxisTranslation = position.SecondaryAxisTranslation + offset * _tubesheet.TubesheetPitch * Math.Sqrt(2);
			return newPos;
		}

		public WalkerPosition OffsetPrimaryBySecondary(WalkerPosition position, int offset) {
			WalkerPosition newPos = position.Clone();
			newPos.SecondaryAxisTranslation = position.SecondaryAxisTranslation - offset * _tubesheet.TubesheetPitch * Math.Sqrt(2);
			if (position.Orientation == ToolHeadOrientation.upLeft) {
				newPos.ToolHeadTube = _tubesheet.GetTube(position.ToolHeadTube.RowIndex - offset, position.ToolHeadTube.ColumnIndex + offset);
			}
			else if (position.Orientation == ToolHeadOrientation.upRight) {
				newPos.ToolHeadTube = _tubesheet.GetTube(position.ToolHeadTube.RowIndex - offset, position.ToolHeadTube.ColumnIndex - offset);
			}
			else if (position.Orientation == ToolHeadOrientation.downRight) {
				newPos.ToolHeadTube = _tubesheet.GetTube(position.ToolHeadTube.RowIndex + offset, position.ToolHeadTube.ColumnIndex - offset);
			}
			else {
				newPos.ToolHeadTube = _tubesheet.GetTube(position.ToolHeadTube.RowIndex + offset, position.ToolHeadTube.ColumnIndex + offset);
			}
			return newPos;
		}

		protected bool CheckOrientation(Tube targetTube, ToolHeadOrientation orientation) { //only primary check
			WalkerPosition p = GetDefaultPosition();
			p.ToolHeadTube = targetTube;
			p.Orientation = orientation;
			return GetPositionOnTubes(p).IsValidPrimary();
		}

		protected IEnumerable<ToolHeadOrientation> GetPossibleOrientations(Tube targetTube) {
			IList<ToolHeadOrientation> ors = new List<ToolHeadOrientation>((IEnumerable<ToolHeadOrientation>)Enum.GetValues(typeof(ToolHeadOrientation)));
			return ors.Where(o => CheckOrientation(targetTube, o)).ToList();
		}

		protected ToolHeadOrientation GetNextOrientation(ToolHeadOrientation orientation, int clockwise) {
			int value = (((int)orientation) + clockwise) % 4;
			if (value < 0) {
				value = 4 + value;
			}
			return (ToolHeadOrientation)value;
		}

		protected int GetTan(ToolHeadOrientation orientation) {
			if (orientation == ToolHeadOrientation.upLeft || orientation == ToolHeadOrientation.downLeft) {
				return -1;
			}
			else {
				return 1;
			}
		}

		protected int GetCtg(ToolHeadOrientation orientation) {
			if (orientation == ToolHeadOrientation.upLeft || orientation == ToolHeadOrientation.upRight) {
				return 1;
			}
			else {
				return -1;
			}
		}

		protected int GetSecondaryAngle(WalkerPosition position) {
			int retAngle = 0;
			if (position.Orientation == ToolHeadOrientation.upLeft) {
				retAngle = position.Rotation == -45 ? 0 : 90;
			}
			else if (position.Orientation == ToolHeadOrientation.upRight) {
				retAngle = position.Rotation == -45 ? 270 : 0;
			}
			else if (position.Orientation == ToolHeadOrientation.downRight) {
				retAngle = position.Rotation == -45 ? 180 : 270;
			}
			else {
				retAngle = position.Rotation == -45 ? 90 : 180;
			}
			return retAngle;
		}

		public int GetCenterOffsetFromTH(WalkerPosition position) {
			int secondaryRowOffset = GetSecondaryOffsetPitches(position);
			int offsetFromTH = secondaryRowOffset + (_walkerModel.PrimaryAxisLengthInPitchesD / 2) + _walkerModel.ToolHeadLengthInPitchesD;
			return offsetFromTH;
		}

		public WalkerPosition RotatePrimary(WalkerPosition position) {
			WalkerPosition newPos = position.Clone();
			int rotationLengthD = GetCenterOffsetFromTH(position);
			int offset = rotationLengthD * 2;
			if (position.Rotation == -45) {
				newPos.Orientation = GetNextOrientation(position.Orientation, 1);
				newPos.Rotation = 45;
			}
			else {
				newPos.Orientation = GetNextOrientation(position.Orientation, -1);
				newPos.Rotation = -45;
			}
			if (GetTan(position.Orientation) == GetTan(newPos.Orientation)) {
				if (GetCtg(position.Orientation) > GetCtg(newPos.Orientation)) {
					offset = -offset;
				}
				newPos.ToolHeadTube = _tubesheet.GetTube(position.ToolHeadTube.RowIndex + offset, position.ToolHeadTube.ColumnIndex);
			}
			else {
				if (GetTan(position.Orientation) > GetTan(newPos.Orientation)) {
					offset = -offset;
				}
				newPos.ToolHeadTube = _tubesheet.GetTube(position.ToolHeadTube.RowIndex, position.ToolHeadTube.ColumnIndex + offset);
			}
			return newPos;
		}

		public bool CanRotateSecondary(WalkerPosition position) {
			WalkerPosition newPos = RotateSecondary(position);
			return IsValidOnT(newPos);
		}

		public WalkerPosition RotateSecondary(WalkerPosition position) {
			WalkerPosition newPos = position.Clone();
			if (newPos.Rotation == -45) {
				newPos.Rotation = 45;
			}
			else {
				newPos.Rotation = -45;
			}
			return newPos;
		}

		public Tube GetOffsetTube(Tube tube, int offsetRow, int offsetColumn) {
			return _tubesheet.GetTube(tube.RowIndex + offsetRow, tube.ColumnIndex + offsetColumn);
		}

		public int GetPrimaryCurrentPossibleOffset(WalkerPosition position, int desiredOffset) {
			int currentOffset = (int)Math.Round(position.PrimaryAxisTranslation / _tubesheet.TubesheetPitch);
			int possiblePositiveOffset = (_walkerModel.SecondaryAxisLengthInPitches / 2) - currentOffset;
			int possibleNegativeOffset = (-_walkerModel.SecondaryAxisLengthInPitches / 2) - currentOffset;
			if (desiredOffset < 0) {
				return Math.Max(desiredOffset, possibleNegativeOffset);
			}
			else {
				return Math.Min(desiredOffset, possiblePositiveOffset);
			}
		}

		public int GetSecondaryCurrentPossibleOffset(WalkerPosition position, int desiredOffset) {
			int currentOffset = (int)Math.Round(position.SecondaryAxisTranslation / _tubesheet.TubesheetPitch / Math.Sqrt(2));
			int possiblePositiveOffset = (_walkerModel.PrimaryAxisLengthInPitchesD / 2) - currentOffset;
			int possibleNegativeOffset = (-_walkerModel.PrimaryAxisLengthInPitchesD / 2) - currentOffset;
			if (desiredOffset < 0) {
				return Math.Max(desiredOffset, possibleNegativeOffset);
			}
			else {
				return Math.Min(desiredOffset, possiblePositiveOffset);
			}
		}

		public bool CanOffsetRows(WalkerPosition position) {
			int sAngle = GetSecondaryAngle(position);
			return sAngle == 90 || sAngle == 270;
		}

		public bool CanOffsetColumns(WalkerPosition position) {
			int sAngle = GetSecondaryAngle(position);
			return sAngle == 0 || sAngle == 180;
		}

		public int GetToolheadRowsDirectedOffset(WalkerPosition position, int offset) {
			if (GetSecondaryAngle(position) == 270) {
				return -offset;
			}
			else {
				return offset;
			}
		}

		public int GetToolheadColumnsDirectedOffset(WalkerPosition position, int offset) {
			if (GetSecondaryAngle(position) == 0) {
				return offset;
			}
			else {
				return -offset;
			}
		}


		public void OffsetToolheadRowsChained(WalkerPath path, int offset) {
			int rowsOffset = GetToolheadRowsDirectedOffset(path.GetLastPosition(), offset);
			OffsetToolheadChained(path, rowsOffset);
		}

		public void OffsetToolheadColumnsChained(WalkerPath path, int offset) {
			int rowsOffset = GetToolheadColumnsDirectedOffset(path.GetLastPosition(), offset);
			OffsetToolheadChained(path, rowsOffset);
		}


		protected bool OffsetSecondaryByPrimary(WalkerPath path, int offset) {

			bool retCode = false;
			WalkerPosition movingPosition = OffsetSecondaryByPrimary(path.GetLastPosition(), offset);
			if (IsValidOnT(movingPosition)) {
				path.AddAction(new TranslatePrimaryAction(-offset) { Position = movingPosition });
				retCode = true;
			}
			return retCode;
		}

		protected bool OffsetPrimaryBySecondary(WalkerPath path, int offset) {

			bool retCode = false;
			WalkerPosition movingPosition = OffsetPrimaryBySecondary(path.GetLastPosition(), offset);
			if (IsValidOnT(movingPosition)) {
				path.AddAction(new TranslateSecondaryAction(-offset) { Position = movingPosition });
				retCode = true;
			}
			return retCode;
		}


		protected bool OffsetPrimary(WalkerPath path, int offset) {
			bool retCode = false;
			WalkerPosition movingPosition = OffsetPrimary(path.GetLastPosition(), offset);
			if (IsValidOnT(movingPosition)) {
				path.AddAction(new TranslatePrimaryAction(offset) { Position = movingPosition });
				retCode = true;
			}
			return retCode;
		}

		protected bool OffsetSecondary(WalkerPath path, int offset) {
			bool retCode = false;
			WalkerPosition movingPosition = OffsetSecondary(path.GetLastPosition(), offset);
			if (IsValidOnT(movingPosition)) {
				path.AddAction(new TranslateSecondaryAction(offset) { Position = movingPosition });
				retCode = true;
			}
			return retCode;
		}

		protected bool RotateSecondary(WalkerPath path) {
			bool retCode = false;
			WalkerPosition movingPosition = RotateSecondary(path.GetLastPosition());
			if (IsValidOnT(movingPosition)) {
				path.AddAction(new RotateSecondaryAction(movingPosition.Rotation * 2) { Position = movingPosition });
				retCode = true;
			}
			return retCode;
		}

		protected bool RotatePrimary(WalkerPath path) {
			bool retCode = false;
			WalkerPosition movingPosition = RotatePrimary(path.GetLastPosition());
			if (IsValidOnT(movingPosition)) {
				path.AddAction(new RotatePrimaryAction(-movingPosition.Rotation * 2) { Position = movingPosition });
				retCode = true;
			}
			return retCode;
		}

		public bool OffsetToolheadChained(WalkerPath path, int offset) {
			int restOffset = offset;
			int stepOffset;
			bool firstTime = true;
			bool running = true;
			int initActionsCount = path.WalkerActions.Count;
			while (Math.Abs(restOffset) > 0 && running) {
				if (!firstTime) {
					int posSecOff = GetPrimaryCurrentPossibleOffset(path.GetLastPosition(), -restOffset);
					running = OffsetSecondaryByPrimary(path, -posSecOff);
				}
				stepOffset = GetPrimaryCurrentPossibleOffset(path.GetLastPosition(), restOffset);
				if (stepOffset != 0) {
					running = running && OffsetPrimary(path, stepOffset);
				}
				if (running) {
					restOffset -= stepOffset;
				}
				firstTime = false;
			}
			return path.WalkerActions.Count > initActionsCount;
		}

		public bool CanRotatePrimary(WalkerPosition position) {
			bool clockwise = position.Rotation == -45;
			return CanRotatePrimary(position, clockwise);
		}

		public bool CanRotatePrimary(WalkerPosition position, bool clockwise) {

			int toolHeadSideLengthD = GetCenterOffsetFromTH(position);
			int otherSideLengthD = _walkerModel.PrimaryAxisLengthInPitchesD + _walkerModel.ToolHeadLengthInPitchesD - toolHeadSideLengthD;
			int rowsCenter;
			int colsCenter;
			if ((position.Orientation == ToolHeadOrientation.upLeft && clockwise) || (position.Orientation == ToolHeadOrientation.upRight && !clockwise)) {
				rowsCenter = position.ToolHeadTube.RowIndex - toolHeadSideLengthD;
				if (rowsCenter < _tubesheet.RowsCount / 2) {
					return rowsCenter - 1 > otherSideLengthD * Math.Sqrt(2);
				}
				else {
					return _tubesheet.RowsCount + 1 - rowsCenter > toolHeadSideLengthD * Math.Sqrt(2);
				}
			}
			if ((position.Orientation == ToolHeadOrientation.downRight && clockwise) || (position.Orientation == ToolHeadOrientation.downLeft && !clockwise)) {
				rowsCenter = position.ToolHeadTube.RowIndex + toolHeadSideLengthD;
				if (rowsCenter < _tubesheet.RowsCount / 2) {
					return _tubesheet.RowsCount + 1 - rowsCenter > toolHeadSideLengthD * Math.Sqrt(2);
				}
				else {
					return rowsCenter - 1 > otherSideLengthD * Math.Sqrt(2);
				}
			}

			if ((position.Orientation == ToolHeadOrientation.upRight && clockwise) || (position.Orientation == ToolHeadOrientation.downRight && !clockwise)) {
				colsCenter = position.ToolHeadTube.ColumnIndex - toolHeadSideLengthD;
				if (colsCenter < _tubesheet.ColumnsCount / 2) {
					return colsCenter - 1 > otherSideLengthD * Math.Sqrt(2);
				}
				else {
					return _tubesheet.ColumnsCount + 1 - colsCenter > toolHeadSideLengthD * Math.Sqrt(2);
				}
			}
			if ((position.Orientation == ToolHeadOrientation.downLeft && clockwise) || (position.Orientation == ToolHeadOrientation.upLeft && clockwise)) {
				colsCenter = position.ToolHeadTube.ColumnIndex + toolHeadSideLengthD;
				if (colsCenter < _tubesheet.ColumnsCount / 2) {
					return _tubesheet.ColumnsCount + 1 - colsCenter > toolHeadSideLengthD * Math.Sqrt(2);
				}
				else {
					return colsCenter - 1 > otherSideLengthD * Math.Sqrt(2);
				}
			}
			return true;
		}

		protected bool TryOffsetD(WalkerPath path, bool forward) {
			bool moved;
			int offset = _walkerModel.AxesDiffInPitches;
			if (!forward) {
				offset = -offset;
			}
			int posOffset = GetSecondaryCurrentPossibleOffset(path.GetLastPosition(), offset);
			moved = OffsetPrimaryBySecondary(path, posOffset);
			int secOffset = GetSecondaryOffsetPitches(path.GetLastPosition());
			moved = moved && OffsetSecondary(path, -secOffset);
			return moved;
		}

		protected bool TrySecondaryToCenter(WalkerPath path, bool forward) {
			bool moved;
			int offset = _walkerModel.AxesDiffInPitches;
			if (!forward) {
				offset = -offset;
			}
			int posOffset = GetSecondaryCurrentPossibleOffset(path.GetLastPosition(), offset);
			moved = OffsetPrimaryBySecondary(path, posOffset);
			int secOffset = GetSecondaryOffsetPitches(path.GetLastPosition());
			moved = moved && OffsetSecondary(path, -secOffset);
			return moved;
		}



		protected bool TryOffset(WalkerPath path, bool forward) {
			bool moved;
			int offset = _walkerModel.SecondaryAxisLengthInPitches;
			if (!forward) {
				offset = -offset;
			}
			int posOffset = GetPrimaryCurrentPossibleOffset(path.GetLastPosition(), offset);
			moved = OffsetSecondaryByPrimary(path, posOffset);
			int secOffset = GetPrimaryOffsetPitches(path.GetLastPosition());
			moved = moved && OffsetPrimary(path, -secOffset);
			return moved;
		}


		public bool OffsetToolheadChainedDiagonal(WalkerPath path, int offset) {
			int restOffset = offset;
			int stepOffset;
			bool firstTime = true;
			bool running = true;
			int initActionsCount = path.WalkerActions.Count;
			while (Math.Abs(restOffset) > 0 && running) {
				if (!firstTime) {
					int posSecOff = GetSecondaryCurrentPossibleOffset(path.GetLastPosition(), restOffset);
					running = OffsetSecondary(path, posSecOff);
				}
				stepOffset = -GetSecondaryCurrentPossibleOffset(path.GetLastPosition(), -restOffset);
				if (stepOffset != 0) {
					running = running && OffsetPrimaryBySecondary(path, stepOffset);
				}
				if (running) {
					restOffset -= stepOffset;
				}
				firstTime = false;
			}
			return path.WalkerActions.Count > initActionsCount;
		}


		public bool IsValidOnT(WalkerPosition position) {
			return position.IsValid() && GetPositionOnTubes(position).IsValid();
		}

		public void TryRotateToOrientation(WalkerPath path, ToolHeadOrientation orientation, bool clockwise) {
			bool running = true;
			if (clockwise) {
				while (running && path.GetLastPosition().Orientation != orientation && IsValidOnT(path.GetLastPosition())) {
					if (path.GetLastPosition().Rotation != -45) {
						running = RotateSecondary(path);
					}
					running = running && CanRotatePrimary(path.GetLastPosition(), clockwise) && !path.IsDead();
					running = running && RotatePrimary(path);
				}
			}
			else {
				while (running && path.GetLastPosition().Orientation != orientation && IsValidOnT(path.GetLastPosition())) {
					if (path.GetLastPosition().Rotation == -45) {
						running = RotateSecondary(path);
					}
					running = running && CanRotatePrimary(path.GetLastPosition(), clockwise) && !path.IsDead();
					running = running && RotatePrimary(path);
				}
			}
		}

		protected WalkerPath Move1D(WalkerPath path) {
			if (path.RowOffset == 0) {
				if (!CanOffsetColumns(path.GetLastPosition())) {
					RotateSecondary(path);
				}
				OffsetToolheadColumnsChained(path, path.ColumnOffset);
			}
			else if (path.ColumnOffset == 0) {
				if (!CanOffsetRows(path.GetLastPosition())) {
					RotateSecondary(path);
				}
				OffsetToolheadRowsChained(path, path.RowOffset);
			}
			return path;
		}

		protected WalkerPath RotateToTarget(WalkerPath path) {
			WalkerPath tryRotateCW = path.Clone();
			TryRotateToOrientation(tryRotateCW, tryRotateCW.TargetOrientation, true);
			WalkerPath tryRotateCCW = path.Clone();
			TryRotateToOrientation(tryRotateCCW, tryRotateCCW.TargetOrientation, false);
			if (tryRotateCW.IsValid && tryRotateCW.IsDirectedToTarget()) {
				path = tryRotateCW;
			}
			if (tryRotateCCW.IsValid && tryRotateCCW.IsDirectedToTarget() && tryRotateCCW.WalkerActions.Count < tryRotateCW.WalkerActions.Count) {
				path = tryRotateCCW;
			}
			return path;
		}

		protected WalkerPath MoveZigZag(WalkerPath path) {
			if (CanOffsetRows(path.GetLastPosition())) {
				WalkerPosition endingPos = path.GetLastPosition().Clone();
				endingPos.ToolHeadTube = GetOffsetTube(path.GetLastPosition().ToolHeadTube, path.RowOffset / 2, 0);
				if (CanRotateSecondary(endingPos)) {
					OffsetToolheadRowsChained(path, path.RowOffset / 2);
					RotateSecondary(path);
					OffsetToolheadColumnsChained(path, path.ColumnOffset);
					RotateSecondary(path);
					OffsetToolheadRowsChained(path, path.RowOffset);
				}
			}
			else {
				WalkerPosition endingPos = path.GetLastPosition().Clone();
				endingPos.ToolHeadTube = GetOffsetTube(path.GetLastPosition().ToolHeadTube, 0, path.ColumnOffset / 2);
				if (CanRotateSecondary(endingPos)) {
					OffsetToolheadColumnsChained(path, path.ColumnOffset / 2);
					RotateSecondary(path);
					OffsetToolheadRowsChained(path, path.RowOffset);
					RotateSecondary(path);
					OffsetToolheadColumnsChained(path, path.ColumnOffset);
				}
			}
			return path;
		}

		public WalkerPath MoveDiagonaly(WalkerPath path) {
			int offsetLength = -Math.Min(Math.Abs(path.ColumnOffset), Math.Abs(path.RowOffset));
			WalkerPosition testPosition = OffsetPrimaryBySecondary(path.GetLastPosition(), offsetLength);
			if (testPosition.ToolHeadTube !=  null && (testPosition.ToolHeadTube.RowIndex == path.TargetTube.RowIndex || testPosition.ToolHeadTube.ColumnIndex == path.TargetTube.ColumnIndex)) {
				if (OffsetToolheadChainedDiagonal(path, offsetLength)) {
					return TryResolvePath(path);
				}
			}
			testPosition = OffsetPrimaryBySecondary(path.GetLastPosition(), -offsetLength);
			if (testPosition.ToolHeadTube !=  null && (testPosition.ToolHeadTube.RowIndex == path.TargetTube.RowIndex || testPosition.ToolHeadTube.ColumnIndex == path.TargetTube.ColumnIndex)) {
				if (OffsetToolheadChainedDiagonal(path, -offsetLength)) {
					return TryResolvePath(path);
				}
			}
			return path;
		}

		public WalkerPath TryResolvePath(WalkerPath path) {
			path.IncrementIterations();
			if (path.IsDone || path.IsDead()) {
				return path;
			}
			if (path.RowOffset == 0 || path.ColumnOffset == 0) {
				return Move1D(path);
			}
			if (!path.IsDirectedToTarget()) {
				path = RotateToTarget(path);
				if (path.IsDirectedToTarget()) {
					return TryResolvePath(path);
				}
			}
			if (path.IsDirectedToTarget()) {
				path = MoveDiagonaly(path);
				if (path.IsDone) {
					return path;
				}
			}
			else if (!path.IsDirectedToTarget()) {
				if (TryOffsetD(path, true) || TryOffsetD(path, false)) {
					path = RotateToTarget(path);
					if (path.IsDirectedToTarget()) {
						return TryResolvePath(path);
					}
					else {
						if (TryOffset(path, true) || TryOffset(path, false)) {
							return TryResolvePath(path);
						}
					}
				}
			}
			if (path.IsDirectedToTarget()) {
				path = MoveZigZag(path);
			}
			if (!path.IsDone) {
				return TryResolvePath(path);
			}
			return path;
		}

		public IEnumerable<WalkerAction> GetWalkerActionsToTarget(WalkerPosition position, Tube targetTube) {
			List<WalkerPath> paths = GetWalkerPaths(position, targetTube).ToList();
			paths.Sort((a, b) => a.ResolvedCost.CompareTo(b.ResolvedCost));
			if (paths.Count() > 0) {
				return paths.First().WalkerActions;
			}
			return null;
		}

		public IEnumerable<WalkerPath> GetWalkerPaths(WalkerPosition position, Tube targetTube) {
			int toolHeadOffsetRows = targetTube.RowIndex - position.ToolHeadTube.RowIndex;
			int toolHeadOffsetColumns = targetTube.ColumnIndex - position.ToolHeadTube.ColumnIndex;
			IEnumerable<ToolHeadOrientation> possibleOrientations = GetPossibleOrientations(targetTube);
			IList<WalkerPath> initialPaths = possibleOrientations.Select(o => new WalkerPath(position, targetTube, o)).ToList();
			IList<WalkerPath> resultPaths = initialPaths.Select(p => TryResolvePath(p)).ToList();
			foreach (WalkerPath p in resultPaths) {
				p.ResolveCost(WalkerActionPricing.GetDefault());
			}
			return resultPaths;
		}
	}
}
