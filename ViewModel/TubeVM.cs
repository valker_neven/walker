﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Walker.Model;
using Walker.Src;


namespace Walker.ViewModel
{
	public class TubeVM : Npc
	{
		protected Tube _tube;
		protected TubeStatus _status;
		public static readonly IEnumerable<string> Statuses = Enum.GetNames(typeof(TubeStatus));
		
		public TubeVM(Tube tube) {
			_tube = tube;
			_status = _tube.Status;
		}

		public object Tag {
			get; set;
		}

		public Tube Tube {
			get { return _tube; }
		}

		public double Left {
			get  { return _tube.Left; }
		}

		public double Top {
			get  { return _tube.Top; }
		}

		public Point Center {
			get  { return _tube.Center; }
		}

		public double Diameter {
			get  { return _tube.Diameter; }
		}

		public double Diamter2X {
			get  { return _tube.Diameter * 2; }
		}

		public string Description {
			get  { return _tube.Description; }
		}

		public bool IsValid {
			get  { return _tube.IsValid; }
		}

		public TubeStatus Status {
			get {
				return _status;
			} 
		}

		public string StatusString {
			get {
				return _status.ToString();
			} 
			set {
				if (Enum.TryParse(value, out TubeStatus stat)) {
					_status = stat;
					_tube.Status = stat;
					RaisePropertyChanged("StatusString");
					RaisePropertyChanged("Status");
				};
			}
		}
	}
}
