﻿using System.Collections.Generic;
using System.Linq;
using Walker.Model;
using Walker.Domain;
using System.Windows.Input;
using Walker.Src;
using System.Collections.ObjectModel;
using System.Windows;

namespace Walker.ViewModel
{

	public class TubesheetVM : Npc
	{
		protected Tubesheet _tubesheetModel;
		protected IEnumerable<TubeVM> _tubesVM;
		protected WalkerPositionVM _positionVM;
		protected TubeVM _targetTube;
		protected WalkerPosition _position;
		protected WalkerPositionOnTubes _positionOnTubes;
		protected WalkerController _walkerController;
		protected TubeVM _selectedTube;

		protected GenericRelayCommand<TubeVM> _SetTubeAsPositionHeadTubeCommand;
		protected GenericRelayCommand<TubeVM> _SetTubeAsWalkerEndTubeCommand;

		protected ICommand _OffsetPrimaryCommand;
		protected ICommand _OffsetSecondaryCommand;
		protected ICommand _OffsetPrimaryBySecondaryCommand;
		protected ICommand _OffsetSecondaryByPrimaryCommand;
		protected ICommand _RotatePrimaryCommand;
		protected ICommand _RotateSecondaryCommand;
		protected ICommand _LoadPathWalkerActionsCommand;
		protected ICommand _LoadWalkerPathsCommand;
		protected ICommand _ResetToDefaultPositionCommand;

		private WalkerAction _pathActionSelectedItem;
		private WalkerPath _pathSelectedItem;

		private ObservableCollection<WalkerAction> _walkerActions = new ObservableCollection<WalkerAction>();
		private ObservableCollection<WalkerPath> _walkerPaths = new ObservableCollection<WalkerPath>();

		public TubesheetVM(Tubesheet tubesheetItem) {
			_tubesheetModel = tubesheetItem;
			_walkerController = new WalkerController(this.GetDefaultWalkerModel(), _tubesheetModel);
			_position = _walkerController.GetDefaultPosition();
			_positionVM = new WalkerPositionVM(_position, _walkerController);
			_positionOnTubes = _walkerController.GetPositionOnTubes(_position);
		}

		public ObservableCollection<WalkerAction> WalkerActions {
			get { 
				return _walkerActions; 
			} 
			set {
				_walkerActions = value;
				RaisePropertyChanged("WalkerActions");
			}
		}

		public ObservableCollection<WalkerPath> WalkerPaths {
			get { 
				return _walkerPaths; 
			} 
			set {
				_walkerPaths = value;
				RaisePropertyChanged("WalkerPaths");
			}
		}

		public ICommand SetTubeAsPositionHeadTubeCommand {
			get {
				if (this._SetTubeAsPositionHeadTubeCommand == null) {
					this._SetTubeAsPositionHeadTubeCommand = new GenericRelayCommand<TubeVM>(o => {
						WalkerActions.Clear();
						WalkerPosition p = WalkerPosition.Position.Clone();
						p.ToolHeadTube = o.Tube;
						if (_walkerController.IsValidOnT(p)) {
							WalkerPosition.Position = p;
						}
					},
						o => true);
				}
				return this._SetTubeAsPositionHeadTubeCommand;
			}
		}

		public ICommand ResetToDefaultPositionCommand {
			get {
				if (this._ResetToDefaultPositionCommand == null) {
					this._ResetToDefaultPositionCommand = new RelayCommand(o => {
						WalkerActions.Clear();
						WalkerPosition p = _walkerController.GetDefaultPosition();
						if (_walkerController.IsValidOnT(p)) {
							WalkerPosition.Position = p;
						}
					},
						o => true);
				}
				return this._ResetToDefaultPositionCommand;
			}
		}

		public ICommand SetTubeAsWalkerEndTubeCommand {
			get {
				if (this._SetTubeAsWalkerEndTubeCommand == null) {
					this._SetTubeAsWalkerEndTubeCommand = new GenericRelayCommand<TubeVM>(o => {
						TargetTube = o;
					},
						o => true);
				}
				return this._SetTubeAsWalkerEndTubeCommand;
			}
		}

		public ICommand OffsetPrimaryCommand {
			get {
				if (this._OffsetPrimaryCommand == null) {
					this._OffsetPrimaryCommand = new RelayCommand(o => {
						WalkerPosition pos = _walkerController.OffsetPrimary(_positionVM.Position, int.Parse((string)o));
						if (pos.IsValid()) {
							WalkerPosition.Position = pos;
						}
					},
						o => {
							int i;
							return int.TryParse((string)o, out i);
						});
				}
				return this._OffsetPrimaryCommand;
			}
		}

		public ICommand OffsetSecondaryCommand {
			get {
				if (this._OffsetSecondaryCommand == null) {
					this._OffsetSecondaryCommand = new RelayCommand(o => {
						WalkerPosition pos = _walkerController.OffsetSecondary(_positionVM.Position, int.Parse((string)o));
						if (pos.IsValid()) {
							WalkerPosition.Position = pos;
						}
					},
						o => {
							int i;
							return int.TryParse((string)o, out i);
						});
				}
				return this._OffsetSecondaryCommand;
			}
		}

		public ICommand OffsetPrimarydBySecondaryCommand {
			get {
				if (this._OffsetPrimaryBySecondaryCommand == null) {
					this._OffsetPrimaryBySecondaryCommand = new RelayCommand(o => {
						WalkerPosition pos = _walkerController.OffsetPrimaryBySecondary(_positionVM.Position, int.Parse((string)o));
						if (pos.IsValid()) {
							WalkerPosition.Position = pos;
						}
					},
						o => {
							return int.TryParse((string)o, out int i);
						});
				}
				return this._OffsetPrimaryBySecondaryCommand;
			}
		}

		public ICommand OffsetSecondaryByPrimaryCommand {
			get {
				if (this._OffsetSecondaryByPrimaryCommand == null) {
					this._OffsetSecondaryByPrimaryCommand = new RelayCommand(o => {
						WalkerPosition pos = _walkerController.OffsetSecondaryByPrimary(_positionVM.Position, int.Parse((string)o));
						if (pos.IsValid()) {
							WalkerPosition.Position = pos;
						}
					},
						o => {
							return int.TryParse((string)o, out int i);
						});
				}
				return this._OffsetSecondaryByPrimaryCommand;
			}
		}
		
		public ICommand RotatePrimaryCommand {
			get {
				if (this._RotatePrimaryCommand == null) {
					this._RotatePrimaryCommand = new RelayCommand(o => {
						WalkerPosition pos = _walkerController.RotatePrimary(_positionVM.Position);
						if (pos.IsValid()) {
							WalkerPosition.Position = pos;
						}
					}, o=> {
						return WalkerPosition.Position != null &&_walkerController.CanRotatePrimary(WalkerPosition.Position);
					});
				}
				return this._RotatePrimaryCommand;
			}
		}

		public ICommand RotateSecondaryCommand {
			get {
				if (this._RotateSecondaryCommand == null) {
					this._RotateSecondaryCommand = new RelayCommand(o => {
						WalkerPosition pos = _walkerController.RotateSecondary(_positionVM.Position);
						if (pos.IsValid()) {
							WalkerPosition.Position = pos;
						}
					}, o=> {
						return WalkerPosition.Position != null &&_walkerController.CanRotateSecondary(WalkerPosition.Position);
					});
				}
				return this._RotateSecondaryCommand;
			}
		}

		public TubeVM TargetTube {
			get {
				return _targetTube;
			}
			set {
				_targetTube = value;
				RaisePropertyChanged("TargetTube");
				RaisePropertyChanged("TargetTubeVisibility");
			}
		}

		public ICommand LoadPathWalkerActionsCommand {
			get {
				if (this._LoadPathWalkerActionsCommand == null) {
					this._LoadPathWalkerActionsCommand = new RelayCommand(o => {
						this.WalkerActions.Clear();
						IEnumerable<WalkerAction> actions = _walkerController.GetWalkerActionsToTarget(WalkerPosition.Position, TargetTube.Tube);
						if (actions != null) {
							foreach (WalkerAction action in actions) {
								this.WalkerActions.Add(action);
							}
						}
					}, o=> {
						return TargetTube != null && TargetTube.Status != TubeStatus.Plugged && WalkerPosition != null;
					});
				}
				return this._LoadPathWalkerActionsCommand;
			}
		}

		public ICommand LoadWalkerPathsCommand {
			get {
				if (this._LoadWalkerPathsCommand == null) {
					this._LoadWalkerPathsCommand = new RelayCommand(o => {
						this.WalkerPaths.Clear();
						IEnumerable<WalkerPath> paths = _walkerController.GetWalkerPaths(WalkerPosition.Position, TargetTube.Tube);
						if (paths != null) {
							foreach (WalkerPath path in paths) {
								this.WalkerPaths.Add(path);
							}
						}
					}, o=> {
						return TargetTube != null && TargetTube.Status != TubeStatus.Plugged && WalkerPosition != null;
					}); 
				}
				return this._LoadWalkerPathsCommand;
			}
		}

		public double TubesheetRealWidth {
			get {
				return _tubesheetModel.TubesheetRealWidth;
			}
		}

		public double TubesheetDiameter {
			get {
				return _tubesheetModel.TubesheetDiameter;
			}
		}

		public double TubesheetRadius {
			get {
				return _tubesheetModel.TubesheetDiameter / 2;
			}
		}

		public double TubesheetSelectionDiameter {
			get {
				return _tubesheetModel.TubesheetDiameter * 1.2;
			}
		}

		public double TubesheetPitch {
			get {
				return _tubesheetModel.TubesheetPitch;
			}
		}

		public Visibility TargetTubeVisibility {
			get {
				return _targetTube != null ? Visibility.Visible : Visibility.Hidden;
			}
		}

		public IEnumerable<TubeVM> Tubes {
			get {
				if (_tubesVM == null) {
					_tubesVM = _tubesheetModel.Tubes.Select(t => new TubeVM(t)).ToList();
				}
				return _tubesVM;
			}
		}

		public WalkerPositionVM WalkerPosition {
			get {
				return _positionVM;
			}
			set {
				_positionVM = value;
				RaisePropertyChanged("WalkerPosition");
			}
		}

		public TubeVM ToolHeadTube {
			get {
				return _positionVM.ToolHeadTube;
			}
			set {
				_positionVM.ToolHeadTube = value;
				RaisePropertyChanged("ToolHeadTube");
			}
		}

		public IEnumerable<string> TubeStatuses {
			get {
				return TubeVM.Statuses;
			}
		}

		public IEnumerable<string> ToolHeadOrientations {
			get {
				return WalkerPositionVM.ToolHeadOrienatations;
			}
		}

		protected WalkerModel GetDefaultWalkerModel() {
			return new WalkerModel(14, 24, 1, _tubesheetModel.TubesheetPitch);
		}

		public WalkerAction PathActionSelectedItem {
			get {
				return _pathActionSelectedItem; 
			}
			set {
				_pathActionSelectedItem = value;
				RaisePropertyChanged("PathActionSelectedItem");
				if (_pathActionSelectedItem != null) {
					WalkerPosition.Position = _pathActionSelectedItem.GetPosition();
				}
			}
		}
		
		public WalkerPath PathSelectedItem {
			get {
				return _pathSelectedItem; 
			}
			set {
				_pathSelectedItem = value;
				RaisePropertyChanged("PathSelectedItem");
			}
		}
	}
}
