﻿using System;
using System.Collections.Generic;
using Walker.Model;
using Walker.Src;
using Walker.Domain;
using System.Windows;

namespace Walker.ViewModel
{
	public class WalkerPositionVM : Npc
	{
		protected WalkerPosition _walkerPosition;
		public static readonly IEnumerable<string> ToolHeadOrienatations = Enum.GetNames(typeof(ToolHeadOrientation));

		protected TubeVM _toolHeadTube;
		protected WalkerController _walkerControler;
		protected WalkerPositionOnTubes _positionOnTubes;

		protected TubeVM _primaryAxisStartTube;
		protected TubeVM _primaryAxisEndTube;
		protected TubeVM _secondaryAxisStartTube;
		protected TubeVM _secondaryAxisEndTube;

		public override string ToString() {
			return ToolHeadTube.Description;
		}

		public ToolHeadOrientation Orientation {
			get {
				return _walkerPosition.Orientation;
			}
		}

		public string OrientationString {
			get {
				return _walkerPosition.Orientation.ToString();
			}
			set {
				if (Enum.TryParse(value, out ToolHeadOrientation orientation)) {
					WalkerPosition p = _walkerPosition.Clone();
					p.Orientation = orientation;
					if (_walkerControler.GetPositionOnTubes(p).IsValid()) {
						_walkerPosition.Orientation = orientation;
						RecalculatePositionOnTubes();
						AllPositionPropertiesChanged();
					}
				};
			}
		}

		public double PrimaryAxisTranslation {
			get {
				return Math.Round(_walkerPosition.PrimaryAxisTranslation, 2);
			}
			set {
				_walkerPosition.PrimaryAxisTranslation = value;
				RecalculatePositionOnTubes();
			}
		}

		public Point PrimaryAxisCenter {
			get {
				return new Point((_primaryAxisEndTube.Center.X + _primaryAxisStartTube.Center.X) / 2, (_primaryAxisEndTube.Center.Y + _primaryAxisStartTube.Center.Y) / 2);
			}
		}

		protected void AllPositionPropertiesChanged() {
			RaisePropertyChanged("OrientationString");
			RaisePropertyChanged("Orientation");
			RaisePropertyChanged("ToolHeadTube");
			RaisePropertyChanged("PrimaryAxisTranslation");
			RaisePropertyChanged("SecondaryAxisTranslation");
			RaisePropertyChanged("PositionOnTubes");
			RaisePropertyChanged("Position");
			RaisePropertyChanged("Rotation");
			RaisePropertyChanged("PrimaryAxisCenter");
			RaisePropertyChanged("PrimaryAxisStartTube");
			RaisePropertyChanged("PrimaryAxisEndTube");
			RaisePropertyChanged("SecondaryAxisStartTube");
			RaisePropertyChanged("SecondaryAxisEndTube");
		}

		public double SecondaryAxisTranslation {
			get {
				return Math.Round(_walkerPosition.SecondaryAxisTranslation, 2);
			}
			set {
				_walkerPosition.SecondaryAxisTranslation = value;
				RecalculatePositionOnTubes();
			}
		}

		public int Rotation {
			get {
				return _walkerPosition.Rotation;
			}
			set {
				_walkerPosition.Rotation = value;
				RecalculatePositionOnTubes();
				RaisePropertyChanged("Rotation");
			}
		}

		public TubeVM ToolHeadTube {
			get {
				return _toolHeadTube;
			}
			set {
				WalkerPosition p = _walkerPosition.Clone();
				p.ToolHeadTube = value.Tube;
				if (_walkerControler.GetPositionOnTubes(p).IsValid()) {
					_toolHeadTube = value;
					_walkerPosition.ToolHeadTube = value.Tube;
					RecalculatePositionOnTubes();
				}
			}
		}

		public void RecalculatePositionOnTubes() {
			_positionOnTubes = _walkerControler.GetPositionOnTubes(_walkerPosition);
			SetTubesVM();
		}

		protected void SetTubesVM() {
			_toolHeadTube = new TubeVM(PositionOnTubes.ToolHeadTube);
			_primaryAxisStartTube = new TubeVM(PositionOnTubes.PrimaryAxisStartTube);
			_primaryAxisEndTube = new TubeVM(PositionOnTubes.PrimaryAxisEndTube);
			_secondaryAxisStartTube = new TubeVM(PositionOnTubes.SecondaryAxisStartTube);
			_secondaryAxisEndTube = new TubeVM(PositionOnTubes.SecondaryAxisEndTube);
			AllPositionPropertiesChanged();
		}

		public WalkerPosition Position {
			get {
				return _walkerPosition;
			}
			set {
				if (_walkerControler.GetPositionOnTubes(value).IsValid()) {
					_walkerPosition = value;
					RaisePropertyChanged("Position");
					RecalculatePositionOnTubes();
				}
			}
		}

		public WalkerPositionOnTubes PositionOnTubes {
			get {
				return _positionOnTubes;
			}
		}

		public TubeVM PrimaryAxisStartTube {
			get {
				return _primaryAxisStartTube;
			}
			set {
				_primaryAxisStartTube = value;
				RaisePropertyChanged("PrimaryAxisStartTube");
			}
		}

		public TubeVM PrimaryAxisEndTube {
			get {
				return _primaryAxisEndTube;
			}
			set {
				_primaryAxisEndTube = value;
				RaisePropertyChanged("PrimaryAxisEndTube");
			}
		}
		public TubeVM SecondaryAxisStartTube {
			get {
				return _secondaryAxisStartTube;
			}
			set {
				_secondaryAxisStartTube = value;
				RaisePropertyChanged("SecondaryAxisStartTube");
			}
		}
		public TubeVM SecondaryAxisEndTube {
			get {
				return _secondaryAxisEndTube;
			}
			set {
				_secondaryAxisEndTube = value;
				RaisePropertyChanged("SecondaryAxisEndTube");
			}
		}

		public WalkerPositionVM(WalkerPosition wp, WalkerController walkerControler) {
			_walkerPosition = wp;
			_walkerControler = walkerControler;
			_toolHeadTube = new TubeVM(wp.ToolHeadTube);
			RecalculatePositionOnTubes();
		}
	}
}
