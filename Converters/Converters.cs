﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Walker.Model;
using Walker.Domain;
using System.Collections.Generic;
using System.Linq;

namespace Walker.Converters
{

	[ValueConversion(typeof(TubeStatus), typeof(Brush))]
	public class TubeStatusToBrushConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
			TubeStatus status = (TubeStatus)value;
			if (status == TubeStatus.Plugged) {
				return new SolidColorBrush(Colors.Gray);
			}
			else if (status == TubeStatus.Critical) {
				return new SolidColorBrush(Colors.Red);
			}
			else {
				return new SolidColorBrush(Colors.White);
			}
		}
		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
			return null;
		}
	}

	[ValueConversion(typeof(int), typeof(Visibility))]
	public class IntToVisibilityConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
			int v = (int)value;
			if (v == 0) {
				return Visibility.Hidden; 
			}
			return Visibility.Visible;
		}
		
		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
			return null;
		}
	}



	public class ArithmeticConverter : IMultiValueConverter
	{
		#region Implementation of IMultiValueConverter

		public static char Multiply = '*';
		public static char Divide = '/';
		public static char Add = '+';
		public static char Subtract = '-';

		public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture) {
			if (values.Length != 3)
				throw new ArgumentException("There should be three values.");

			if (string.IsNullOrEmpty(values[0].ToString())) {
				values[0] = "0";
			}

			if (string.IsNullOrEmpty(values[2].ToString())) {
				values[2] = "0";
			}

			double x;
			if (!double.TryParse(values[0].ToString(), out x)) {
				throw new ArgumentException("values[0] must parse to double");
			}

			double y;
			if (!double.TryParse(values[2].ToString(), out y)) {
				throw new ArgumentException("values[0] must parse to double");
			}

			char op = (char)values[1];
			if (op == Multiply)
				return (x * y);
			if (op == Divide)
				return (x / y);
			if (op == Add)
				return (x + y);
			if (op == Subtract)
				return (x - y);

			throw new ArgumentException("Unknown operator.");
		}

		public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture) {
			throw new NotImplementedException();
		}

		#endregion
	}


	public class MathConverter : IValueConverter
	{
		private static readonly char[] _allOperators = new[] { '+', '-', '*', '/', '%', '(', ')' };

		private static readonly List<string> _grouping = new List<string> { "(", ")" };
		private static readonly List<string> _operators = new List<string> { "+", "-", "*", "/", "%" };

		#region IValueConverter Members

		public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
			// Parse value into equation and remove spaces
			var mathEquation = parameter as string;
			mathEquation = mathEquation.Replace(" ", "");
			mathEquation = mathEquation.Replace("@VALUE", value.ToString());

			// Validate values and get list of numbers in equation
			var numbers = new List<double>();
			double tmp;

			foreach (string s in mathEquation.Split(_allOperators)) {
				if (s != string.Empty) {
					if (double.TryParse(s, out tmp)) {
						numbers.Add(tmp);
					}
					else {
						// Handle Error - Some non-numeric, operator, or grouping character found in string
						throw new InvalidCastException();
					}
				}
			}

			// Begin parsing method
			EvaluateMathString(ref mathEquation, ref numbers, 0);

			// After parsing the numbers list should only have one value - the total
			return numbers[0];
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
			throw new NotImplementedException();
		}

		#endregion

		// Evaluates a mathematical string and keeps track of the results in a List<double> of numbers
		private void EvaluateMathString(ref string mathEquation, ref List<double> numbers, int index) {
			// Loop through each mathemtaical token in the equation
			string token = GetNextToken(mathEquation);

			while (token != string.Empty) {
				// Remove token from mathEquation
				mathEquation = mathEquation.Remove(0, token.Length);

				// If token is a grouping character, it affects program flow
				if (_grouping.Contains(token)) {
					switch (token) {
						case "(":
							EvaluateMathString(ref mathEquation, ref numbers, index);
							break;

						case ")":
							return;
					}
				}

				// If token is an operator, do requested operation
				if (_operators.Contains(token)) {
					// If next token after operator is a parenthesis, call method recursively
					string nextToken = GetNextToken(mathEquation);
					if (nextToken == "(") {
						EvaluateMathString(ref mathEquation, ref numbers, index + 1);
					}

					// Verify that enough numbers exist in the List<double> to complete the operation
					// and that the next token is either the number expected, or it was a ( meaning
					// that this was called recursively and that the number changed
					if (numbers.Count > (index + 1) &&
						(double.Parse(nextToken) == numbers[index + 1] || nextToken == "(")) {
						switch (token) {
							case "+":
								numbers[index] = numbers[index] + numbers[index + 1];
								break;
							case "-":
								numbers[index] = numbers[index] - numbers[index + 1];
								break;
							case "*":
								numbers[index] = numbers[index] * numbers[index + 1];
								break;
							case "/":
								numbers[index] = numbers[index] / numbers[index + 1];
								break;
							case "%":
								numbers[index] = numbers[index] % numbers[index + 1];
								break;
						}
						numbers.RemoveAt(index + 1);
					}
					else {
						// Handle Error - Next token is not the expected number
						throw new FormatException("Next token is not the expected number");
					}
				}

				token = GetNextToken(mathEquation);
			}
		}

		// Gets the next mathematical token in the equation
		private string GetNextToken(string mathEquation) {
			// If we're at the end of the equation, return string.empty
			if (mathEquation == string.Empty) {
				return string.Empty;
			}

			// Get next operator or numeric value in equation and return it
			string tmp = "";
			foreach (char c in mathEquation) {
				if (_allOperators.Contains(c)) {
					return (tmp == "" ? c.ToString() : tmp);
				}
				else {
					tmp += c;
				}
			}

			return tmp;
		}
	}
}
