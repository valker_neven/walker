﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Walker.Model
{

	public abstract class WalkerAction
	{
		public WalkerPosition Position;
		public abstract double GetCost(WalkerActionPricing pricing);
		public abstract string GetDescription();
		public abstract string GetCode();

		public virtual bool ShouldRegisterPosition() {
			return true;
		}
		
		public string LongDescription {
			get{
				return $"P: {Position.Description} Action: {this.GetDescription()}";
			}
		}

		public WalkerPosition GetPosition() {
			return Position;
		}

	}
	
	public class TranslatePrimaryAction : WalkerAction // slide on secondary
	{
		public int Offset;
		
		public TranslatePrimaryAction(int offset) {
			Offset = offset;
		}

		public override double GetCost(WalkerActionPricing pricing) {
			return Math.Sqrt(2) * Math.Abs(Offset) * pricing.TranslatePitchPrice;
		}

		public override string GetDescription() {
			return $"Offset primary Axis by {Offset} tubes";
		}

		public override string GetCode() {
			return $"PRIMARY#OFFSET#{Offset}";
		}

	}
	public class TranslateSecondaryAction : WalkerAction
	{
		protected int Offset { get; }
		
		public TranslateSecondaryAction(int offset) {
			Offset = offset;
		}

		public override double GetCost(WalkerActionPricing pricing) {
			return Math.Abs(Offset) * pricing.TranslatePitchPrice;
		}

		public override string GetDescription() {
			return $"Offset secondary Axis by {Offset} tubes";
		}

		public override string GetCode() {
			return $"SECONDARY#OFFSET#{Offset}";
		}

	}

	public class RotatePrimaryAction : WalkerAction
	{
		public int Rotation { get; set; }
		public RotatePrimaryAction(int rotation) {
			Rotation = rotation;
		}

		public override double GetCost(WalkerActionPricing pricing) {
			return Math.Abs(Rotation) * pricing.RotatePrimaryPrice;
		}

		public override string GetDescription() {
			return $"Rotate primary axis by {Rotation}";
		}

		public override string GetCode() {
			return $"PRIMARY#ROTATE#{Rotation}";
		}
	}

	public class RotateSecondaryAction : WalkerAction
	{
		public int Rotation { get; set; }
		public RotateSecondaryAction(int rotation) {
			Rotation = rotation;
		}

		public override double GetCost(WalkerActionPricing pricing) {
			return Math.Abs(Rotation) * pricing.RotateSecondaryPrice;
		}

		public override string GetDescription() {
			return $"Rotate secondary axis by {Rotation}";
		}

		public override string GetCode() {
			return $"SECONDARY#ROTATE#{Rotation}";
		}
	}

	public class LockPrimaryAction : WalkerAction
	{
		public override double GetCost(WalkerActionPricing pricing) {
			return pricing.LockPincersPrice;
		}

		public override string GetDescription() {
			return $"Lock primary axis";
		}

		public override string GetCode() {
			return $"PRIMARY#LOCK";
		}

		public override bool ShouldRegisterPosition() {
			return false;
		}

	}

	public class LockSecondaryAction : WalkerAction
	{
		public override double GetCost(WalkerActionPricing pricing) {
			return pricing.LockPincersPrice;
		}

		public override string GetDescription() {
			return $"Lock secondary axis";
		}

		public override string GetCode() {
			return $"SECONDARY#LOCK";
		}

		public override bool ShouldRegisterPosition() {
			return false;
		}

	}

	public class UnlockSecondaryAction : WalkerAction
	{
		public override double GetCost(WalkerActionPricing pricing) {
			return pricing.LockPincersPrice;
		}

		public override string GetDescription() {
			return $"Unlock secondary axis";
		}

		public override string GetCode() {
			return $"SECONDARY#UNLOCK";
		}

		public override bool ShouldRegisterPosition() {
			return false;
		}
	}

	public class UnlockPrimaryAction : WalkerAction
	{
		public override double GetCost(WalkerActionPricing pricing) {
			return pricing.LockPincersPrice;
		}

		public override string GetDescription() {
			return $"Unlock primary axis";
		}

		public override string GetCode() {
			return $"PRIMARY#UNLOCK";
		}
	
		public override bool ShouldRegisterPosition() {
			return false;
		}
	}

	public class IdleAction : WalkerAction
	{
		public override double GetCost(WalkerActionPricing pricing) {
			return 0;
		}

		public override string GetDescription() {
			return $"Idle";
		}

		public override string GetCode() {
			return "IDLE";
		}
	}

	public class WalkerPath
	{
		protected List<WalkerAction> _walkerActions;
		public IReadOnlyCollection<WalkerAction> WalkerActions { get{ return _walkerActions.AsReadOnly(); } }
		public WalkerPosition StartPosition { get; }
		public Tube TargetTube { get; }
		public ToolHeadOrientation TargetOrientation { get; }
		protected double _resolvedCost;
		protected int _iterationsCount;
		protected Dictionary<string, WalkerAction> _positions;
		protected bool _closed;	

		public string Description { 
			get { return $"Done: {IsDone}, Or.; {TargetOrientation}; Actions: {WalkerActions.Count}, Cost: {Math.Round(ResolvedCost, 1)}"; }
		}	

		private static readonly int _emptyIterationsTolerance = 10; 

		public WalkerPath(WalkerPosition startPosition, Tube targetTube, ToolHeadOrientation targetOrientation) {
			_walkerActions = new List<WalkerAction>();
			_positions = new Dictionary<string, WalkerAction>();
			TargetTube = targetTube;
			TargetOrientation= targetOrientation;
			StartPosition = startPosition;
			_iterationsCount = 0;
			_closed = false;
			AddAction(new IdleAction() {
				Position = startPosition
			});
		}

		public void AddAction(WalkerAction action) {
			if (CheckPosition(action)) {
				_walkerActions.Add(action);
				_iterationsCount = _walkerActions.Count;
			}
		}

		protected bool CheckPosition(WalkerAction action) {
			if (action.ShouldRegisterPosition()) {
				if (_positions.ContainsKey(action.Position.Code)) {
					_closed = true;
					return false;
				}
				_positions.Add(action.Position.Code, action);
			}
			return true;
		}

		public WalkerPath Clone(){
			return new WalkerPath(StartPosition, TargetTube, TargetOrientation) {
				_walkerActions = new List<WalkerAction>(WalkerActions),
				_iterationsCount = this._iterationsCount,
				_positions = this._positions,
				_closed = this._closed
			};
		}

		public void IncrementIterations() { 
			_iterationsCount += 1;
		}

		public bool IsDead() {
			return _closed || _iterationsCount - WalkerActions.Count > _emptyIterationsTolerance;
		}

		public bool IsRepeating() {
			return _iterationsCount - WalkerActions.Count > _emptyIterationsTolerance / 2;
		}

		public WalkerPosition GetLastPosition() {
			return _walkerActions[WalkerActions.Count - 1].GetPosition();
		}

		public bool IsValid {
			get {
				return GetLastPosition().IsValid();
			}
		}

		public bool IsDone {
			get {
				WalkerPosition pos = GetLastPosition();
				return pos.IsValid() && pos.ToolHeadTube == TargetTube;
			}
		}

		public  double ResolvedCost {
			get {return _resolvedCost; }	
		}	
		
		public void ResolveCost(WalkerActionPricing pricing) {
			_resolvedCost = WalkerActions.Select(act => act.GetCost(pricing)).Sum();
		}	


		public int RowOffset {
			get { return TargetTube.RowIndex - GetLastPosition().ToolHeadTube.RowIndex; }
		}

		public int ColumnOffset {
			get { return TargetTube.ColumnIndex - GetLastPosition().ToolHeadTube.ColumnIndex; }
		}


		public IEnumerable<WalkerPosition> GetPositions() {
			return WalkerActions.Select(wa => wa.GetPosition());
		}

		public int GetRestRowOffset() {
			return TargetTube.RowIndex - GetLastPosition().ToolHeadTube.RowIndex;
		}

		public int GetRestClumnOffset() {
			return TargetTube.ColumnIndex - GetLastPosition().ToolHeadTube.ColumnIndex;
		}

		public bool IsDirectedToTarget() {
			return GetLastPosition().Orientation == TargetOrientation;
		}

		public bool IsOppositeToTarget() {
			return Math.Abs((int)GetLastPosition().Orientation - (int)TargetOrientation) == 2;
		}

	}
}
