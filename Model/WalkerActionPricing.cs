﻿
namespace Walker.Model
{
	public class WalkerActionPricing
	{
		public double TranslatePitchPrice { get; set; }
		public double RotatePrimaryPrice { get; set; }
		public double RotateSecondaryPrice { get; set; }
		public double LockPincersPrice { get; set; }
	
		public static WalkerActionPricing GetDefault() {
			return new WalkerActionPricing() {
				TranslatePitchPrice = 1,
				RotatePrimaryPrice = 1,
				RotateSecondaryPrice = 1,
				LockPincersPrice = 1
			};
		}
	}
}
