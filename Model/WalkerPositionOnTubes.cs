﻿
namespace Walker.Model

{
	public class WalkerPositionOnTubes
	{
		protected WalkerPosition _position;


		public Tube PrimaryAxisStartTube {
			get;
		}

		public Tube PrimaryAxisEndTube {
			get;
		}

		public Tube SecondaryAxisStartTube {
			get;
		}

		public Tube SecondaryAxisEndTube {
			get;
		}

		public Tube ToolHeadTube {
			get {
				return _position.ToolHeadTube;
			}
		}

		public WalkerPosition GetWalkerPosition() {
			return _position.Clone();
		}

		public WalkerPositionOnTubes(WalkerPosition position, Tube mainAxT1, Tube mainAxT2, Tube secAxT1, Tube secAxT2) {
			PrimaryAxisStartTube = mainAxT1;
			PrimaryAxisEndTube = mainAxT2;
			SecondaryAxisStartTube = secAxT1;
			SecondaryAxisEndTube = secAxT2;
			_position = position;
		}

		public WalkerPositionOnTubes Clone() {
			return new WalkerPositionOnTubes(
					_position,
					PrimaryAxisStartTube,
					PrimaryAxisEndTube,
					SecondaryAxisStartTube,
					SecondaryAxisEndTube
				);
		}

		public bool IsValid() {
			return
				PrimaryAxisStartTube != null &&
				PrimaryAxisEndTube != null &&
				SecondaryAxisStartTube != null &&
				SecondaryAxisEndTube != null &&
				ToolHeadTube != null &&
				PrimaryAxisStartTube.Status != TubeStatus.Plugged &&
				PrimaryAxisEndTube.Status != TubeStatus.Plugged &&
				SecondaryAxisStartTube.Status != TubeStatus.Plugged &&
				SecondaryAxisStartTube.Status != TubeStatus.Plugged;
		}

		public bool IsValidPrimary() {
			return
				_position.IsValid() &&
				PrimaryAxisStartTube != null &&
				PrimaryAxisEndTube != null &&
				ToolHeadTube != null &&
				PrimaryAxisStartTube.Status != TubeStatus.Plugged &&
				PrimaryAxisEndTube.Status != TubeStatus.Plugged;
		}

		public bool CanLockPrimary() {
			return 
				PrimaryAxisStartTube.Status != TubeStatus.Plugged &&
				PrimaryAxisEndTube.Status != TubeStatus.Plugged;
		}

		public bool CanLockSecondary() {
			return 
				PrimaryAxisStartTube.Status != TubeStatus.Plugged &&
				PrimaryAxisEndTube.Status != TubeStatus.Plugged;
		}
	}
}