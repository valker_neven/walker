﻿using System.Windows;

namespace Walker.Model
{
	public enum TubeStatus
	{
		Unknown,
		Plugged,
		Critical
	}

	public class Tube
	{
		public Tube(int rowIndex, int columnIndex, TubeStatus status) {
			RowIndex = rowIndex;
			ColumnIndex = columnIndex;
			Status = status;
		}

		public int RowIndex {
			get;
		}
		public int ColumnIndex {
			get;
		}
		public TubeStatus Status {
			get; set;
		}

		public Point Center {
			get; set;
		}

		public double Diameter {
			get; set;
		}

		public double Left {
			get  { return Center.X - Diameter * 0.5; }
		}

		public double Top {
			get  { return Center.Y - Diameter * 0.5; }
		}

		public string Description {
			get{ return GetDescription(); }
		}

		public override string ToString() {
			return ($"R{RowIndex}:C{ColumnIndex}");
		}

		public string GetDescription() {
			if(RowIndex == 0) {
				return "";
			}
			return ($"R{RowIndex}:C{ColumnIndex}");
		}

		public bool IsValid  {
			get { return RowIndex > 0; }
		}
	}


}