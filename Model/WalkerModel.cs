﻿using System;

namespace Walker.Model
{

	public class WalkerModel
	{
		public WalkerModel(int primaryAxisLengthInPitchesD, int secondaryAxisLengthInPitches, int toolHeadLengthInPitchesD, double pitchLength) {
			PrimaryAxisLengthInPitchesD = primaryAxisLengthInPitchesD;
			PrimaryAxisLength = primaryAxisLengthInPitchesD * Math.Sqrt(2) * pitchLength;

			SecondaryAxisLengthInPitches = secondaryAxisLengthInPitches;
			SecondaryAxisLength = secondaryAxisLengthInPitches * pitchLength;
			ToolHeadLengthInPitchesD = toolHeadLengthInPitchesD;
			ToolHeadLength = toolHeadLengthInPitchesD * Math.Sqrt(2) * pitchLength;
			AxesDiffInPitches = (int)(Math.Ceiling((SecondaryAxisLength - PrimaryAxisLength) / pitchLength));
		}

		public int PrimaryAxisLengthInPitchesD {
			get;
		}
		public int SecondaryAxisLengthInPitches {
			get;
		}
		public int ToolHeadLengthInPitchesD {
			get;
		}

		public int AxesDiffInPitches {
			get;
		}

		public double PrimaryAxisLength {
			get;
		}
		public double SecondaryAxisLength {
			get;
		}
		public double ToolHeadLength {
			get;
		}


	}
}