﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;

namespace Walker.Model
{


	public class Tubesheet
	{
		protected Tube[,] _tubesGrid;
		protected IList<Tube> _flatTubes;

		public double TubesheetDiameter {
			get;
		}
		public double TubesheetPitch {
			get;
		}

		public double TubesheetRealWidth {
			get;
		}

		public double TubesheetRealHeight {
			get;
		}

		public int RowsCount {
			get;
		}
		
		public int ColumnsCount {
			get;
		}


		public Tubesheet(IEnumerable<Tube> tubes, double tubesheetDiameter, double  tubesheetPitch) {

			TubesheetDiameter = tubesheetDiameter;
			TubesheetPitch = tubesheetPitch;
			RowsCount = tubes.Select(t => t.RowIndex).Max();
			ColumnsCount = tubes.Select(t => t.ColumnIndex).Max();
			_tubesGrid = new Tube[RowsCount, ColumnsCount];
			foreach (Tube t in tubes) {
				_tubesGrid[t.RowIndex - 1, t.ColumnIndex - 1] = t;
			}
			TubesheetRealWidth = (ColumnsCount + 1) * TubesheetPitch;
			TubesheetRealHeight = (RowsCount + 1) * TubesheetPitch;
			ResolveTubesCoordinates();
		}

		protected void ResolveTubesCoordinates() {
			_flatTubes = new List<Tube>();
			for (int i = 0; i < RowsCount; i++) {
				for (int j = 0; j < ColumnsCount; j++) {
					double x = (TubesheetPitch * 0.5) + j * TubesheetPitch;
					double y = (TubesheetPitch * 0.5) + (RowsCount - i - 1) * TubesheetPitch; 
					_tubesGrid[i, j].Center = new Point(x, y);
					_tubesGrid[i, j].Diameter = TubesheetDiameter;
					_flatTubes.Add(_tubesGrid[i, j]);
				}
			}
		}

		public Tube GetTube(int rowIndex, int columnIndex) {
			if (rowIndex > 0 && rowIndex <= RowsCount && columnIndex > 0 && columnIndex <= ColumnsCount) {
				return _tubesGrid[rowIndex - 1, columnIndex - 1];
			}
			return null;
		}

		public IEnumerable<Tube> Tubes {
			get {
				return _flatTubes;
			}
		}
			
		public bool IsValid() {
			for (int i = 1; i <= RowsCount; i++) {
				for (int j = 1; j <= ColumnsCount; j++) {
					if (GetTube(i, j) == null) {
						return false;
					}
				}
			}
			return true;
		}
	}


}
