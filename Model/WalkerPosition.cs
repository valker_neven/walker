﻿using System;

namespace Walker.Model

{
	
	public enum ToolHeadOrientation {
		upLeft = 0,
		upRight = 1,
		downRight = 2,
		downLeft = 3
	}
	
	public class WalkerPosition {
		
		public ToolHeadOrientation Orientation{ get; set;}
		public double PrimaryAxisTranslation{ get; set;}
		public double SecondaryAxisTranslation{ get; set;}
		public int Rotation { get; set;}
		public Tube ToolHeadTube { get; set; }
		protected WalkerModel _walkerModel;

		public bool PrimaryLocked{ get; set;}
		public bool SecondaryLocked{ get; set;}


		public WalkerPosition (WalkerModel walkerModel) {
			_walkerModel = walkerModel;
		}

		public WalkerPosition Clone() {
			return new WalkerPosition(_walkerModel){
				Orientation = Orientation,
				PrimaryAxisTranslation = PrimaryAxisTranslation,
				SecondaryAxisTranslation = SecondaryAxisTranslation,
				Rotation = Rotation,
				ToolHeadTube = ToolHeadTube
			};
		}

		public bool IsValid() {
			return ToolHeadTube != null &&
			L_EQ_T(Math.Abs(PrimaryAxisTranslation), _walkerModel.SecondaryAxisLength / 2, 0.001) && L_EQ_T(Math.Abs(SecondaryAxisTranslation), _walkerModel.PrimaryAxisLength / 2, 0.001);
		}

		private bool L_EQ_T(double a, double b, double tolerance) { //LessEqual with tolerance
			return (a - tolerance) < b;
		}

		public string Description {
			get { return ToolHeadTube.Description; }
		}

		public string Code {
			get { 
				return $"{ToolHeadTube.Description}#{Rotation}#{PrimaryAxisTranslation}#{SecondaryAxisTranslation}#{Orientation}";
			}
		}

	}

}