﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Walker.Model;
using Walker.Domain;
using Walker.ViewModel;

namespace Walker
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		public TubesheetVM TubesheetVM { get; }
		
		public MainWindow() {
			InitializeComponent();
			ITubesheetLoader loader = new TubesheetXMLLoader(@"..\..\input\Tubesheet.txt");
			Tubesheet ts = loader.LoadTubesheet();
			TubesheetVM = new TubesheetVM(ts);
			DataContext = TubesheetVM;
		}
	}
}
